

;Speed Bracer
;Group G7's COMP2121 Project
;Members: Kevin Lerdwichagul, Andrew Gosali, Jeff so
;Date finished: 6/6/2014
;A game using the development board where the user may play a game of
;"Speed Bracer" On the LCD Screen using the keypd as input and various
;external sources for ouput. (See game manual for how to play)

; It is assumed that the following connections on the board are made:
;(Keypad) RO-C3 -> PC0-PC7
;(LCD) D0-D7 -> PA0-PA7
;(LCD) RW-RS -> PB0-PB3
;Mot -> PB7
;PB0 -> PD0
;PB1 -> PD1
;(LED) 0-3 -> PE7-PE4 (reversed)
;(LED) 4 -> PE3
;(LED) 5 -> PE2
;(LED) 6-9 -> PD4-PD7


	.include "m64def.inc"
	.def temp =r16 ;General registers for temporary storage of values
	.def temp2 =r17
	.def count = r20 ;Count register, used in timing related functions
	.def row =r18 ;Row keypad register for keypad
	.def col =r19 ;Column position register for keypad
	.def mask =r21
	.def data = r22 ;register holds value for value to be displayed on screen
	.def randomNum = r23 ;Register used for random number generation - later used for random object generation
	.def temp3 = r24
	.def score =r25 ;Used for score on LED
	.def score2 = r26


	;Stack variable locations
	;Stack positions 1-32 specify the exact location on the LCD. Where unique locations are used below
	.equ LEVEL = 3
	.equ LIVES = 7
	.equ UNITS = 23
	.equ TENS = 22
	.equ HUNDREDS = 21
	.equ THOUSANDS = 20
	.equ TENTHOUSANDS = 19

	;The following are all data-control and storage positions in the stack. See Desig manual for their elaboration
	.equ CARPOS= 33
	.equ OBJ1POS = 34 ;Object means obstacle
	.equ OBJ2POS = 35
	.equ OBJ3POS = 36
	.equ OBJ4POS = 37
	.equ POW1POS = 38
	.equ POW2POS = 39
	.equ POW3POS = 40
	.equ POW4POS = 41
	.equ OBJCAP = 42 ;Tells the program to stop looking for objects once reache the cap. Store a "Y" in here

	.equ COUNTER1 = 43
	.equ COUNTER2 = 44
	.equ COUNTER3 = 45
	.equ COUNTER4 = 46
	.equ COUNTER5 = 47
	.equ COUNTER6 = 48
	.equ GENOBJFLAG = 49
	.equ LEVELTIMER=50
	.equ PERIOD_LOW = 51
	.equ PERIOD_HIGH = 52
	.equ LEVEL_VALUE = 53
	.equ LIVES_VALUE = 54
	.equ PB1_COUNT = 55
	.equ MOTOR_TIMER = 56
	.equ MOTOR_FLAG = 57
	.equ STACKTOP = 58

	.equ SCORE_MASK = 0b11111100 ;This mask is because for the LED we are only using portE
								 ;pins PE2-PE7
	.equ SCORE_MASK2 = 0b11000000 ;The mask for the remaining two bits for LED - PD4 and PD5


	.equ PORTCDIR = 0xF0
	.equ INITCOLMASK = 0xEF
	.equ INITROWMASK = 0x01
	.equ ROWMASK = 0x0F



	;LCD protocol control bits
	.equ LCD_RS = 3
	.equ LCD_RW = 1
	.equ LCD_E = 2
	;LCD functions
	.equ LCD_FUNC_SET = 0b00110000
	.equ LCD_DISP_OFF = 0b00001000
	.equ LCD_DISP_CLR = 0b00000001
	.equ LCD_DISP_ON = 0b00001100
	.equ LCD_ENTRY_SET = 0b00000100
	.equ LCD_ADDR_SET = 0b10000000
	;LCD function bits and constants
	.equ LCD_BF = 7
	.equ LCD_N = 3
	.equ LCD_F = 2
	.equ LCD_ID = 1
	.equ LCD_S = 0
	.equ LCD_C = 1
	.equ LCD_B = 0
	.equ LCD_LINE1 = 0
	.equ LCD_LINE2 = 0x40

	.equ RAND_A = 214013
	.equ RAND_C = 2531011

	.dseg
	.org 0x100
	RAND: .byte 4

	.cseg


	.MACRO delay ;Delays the program by numbers stored in temp and temp2
	loop:

	subi temp, 1
	sbci temp2, 0
	nop
	nop
	nop
	nop
	brne loop
	; taken branch takes two cycles.
	; one loop time is 8 cycles = ~1.08us
	.ENDMACRO

	.org 0x0000
	jmp RESET
	jmp PB_0 ; IRQ0 Handler - Push button 0
	jmp PB_1 ; IRQ1 Handler - Push Button 1
	jmp Default ; IRQ2 Handler
	jmp Default ; IRQ3 Handler
	jmp Default; IRQ4 Handler
	jmp Default ; IRQ5 Handler
	jmp Default ; IRQ6 Handler	
	jmp Default ; IRQ7 Handler
	jmp Default ; Timer2 Compare Handler
	jmp Timer2 ; Timer2 Overflow Handler
	jmp Default ; Timer1 Capture Handler
	jmp Default ; Timer1 CompareA Handler
	jmp Default ; Timer1 CompareB Handler
	jmp Default; Timer1 Overflow Handler
	jmp Default ; Timer0 Compare Handler
	jmp Timer0 ; Timer0 Overflow Handler

	Default: reti


	RESET:

	ldi temp, low(RAMEND - STACKTOP)  ;stack and Y pointers now point to STACKTOP from RAMEND
	out SPL, temp
	ldi temp, high(RAMEND - STACKTOP) 
	out SPH, temp

	in r28,SPL    ;reading in stack pointer addresses into registers
	in r29,SPH

	mov ZL, YL ; z points to the start of the stack
	mov ZH,YH

	; Init score, level and car lives
	ldi temp,'1'
	std Y+LEVEL_VALUE, temp
	ldi temp,'3'
	std Y+LIVES_VALUE, temp

	;Init LED score			
	ser temp
	out DDRE, temp
	ldi temp, 0b11110000
	out DDRD, temp
	clr temp
	out PORTE, temp
	out PORTD, temp
	ldi score, 0
	ldi score2, 0

;	jmp nextLevel
	push ZL
	push ZH


	;init for motor
	clr temp
	std Y+MOTOR_TIMER, temp
	std Y+MOTOR_FLAG, temp

	;INIT for Timer0
	ldi temp, 35
	std Y+PERIOD_HIGH, temp
	ldi temp, 97
	std Y+PERIOD_LOW , temp

	;INIT for pushbutton0 and pushbutton1
	ldi temp, (1 << ISC01) | (1<<ISC11) ;setting the interrupts for falling edge
	sts EICRA, temp                       ;storing them into EICRA
	in temp, EIMSK                        ;taking the values inside the EIMSK  
	ori temp, (1<<INT0) | (1<<INT1)
	out EIMSK, temp
	nop
	nop


	;INIT for timer1 - nextlevel
	ldi temp,0
	std Y+LEVELTIMER, temp

	;INIT For keyboard
	ldi temp, PORTCDIR ; columns are outputs, rows are inputs
	out DDRC, temp

	;Init the LCD board and stack
	clr count

	rcall lcd_init
	rcall initBoard
	rcall resetBoard
	
	ldi temp, '0'
	std Y+UNITS, temp
	
	std Y+TENS, temp
	std Y+THOUSANDS, temp
	std Y+TENTHOUSANDS,temp
	std Y+HUNDREDS, temp


	rcall show_LCD

	sei ;Set global interupt
	pop ZH
	pop ZL

	;ldi temp,6
	;jmp convert_end


	;TIMER FOR RANDOM NUMBER:
	ldi temp, 1 << CS10 ;Start timer
	out TCCR1B, temp


					;Init timer 2
	ldi temp, 0b00000010 ;
	out TCCR2, temp ; Prescaling value=8 ;256*8/7.3728( Frequency of the clock 7.3728MHz, for the overflow it should go for 256 times)


	;Init timer 0
	ldi temp, 0b00000010 ;
	out TCCR0, temp ; Prescaling value=8 ;256*8/7.3728( Frequency of the clock 7.3728MHz, for the overflow it should go for 256 times)
	ldi temp, 1<<TOIE0 ; =278 microseconds
	ori temp, 1<<TOIE2 ; =278 microseconds

	out TIMSK, temp ; T/C0 interrupt enable
	

	jmp main


	;***************Board functions******************
	restartLevel:

	rcall lcd_init
	rcall initBoard
	rcall resetBoard	
	rcall show_LCD

	ret

	initBoard:
	push ZL
	push ZH
	push YL
	push YH
	push count
	push temp
	push temp2
	
	mov ZL, YL
	mov ZH, YH
	clr count
	clr temp
	clr temp2
	
	initBoardLoop: ;Put empty spaces in each lcd board position first
	inc count ;increment till reach 32
	adiw Z, 1

	;Maintain the score even if level is reset.
	cpi count, UNITS
	breq initBoardLoop
	cpi count, TENS
	breq initBoardLoop
	cpi count, HUNDREDS
	breq initBoardLoop
	cpi count, THOUSANDS
	breq initBoardLoop
	cpi count, TENTHOUSANDS
	breq initBoardLoop


	ldi temp, ' '
	st Z, temp
	
	
	cpi count, 33
	brlo initBoardLoop

	pop temp2
	pop temp
	pop count
	pop YH
	pop YL
	pop ZH
	pop ZL
	ret 


	resetBoard: ;Reset the STACK-data and LCD values for the board.
	push ZL
	push ZH
	push YL
	push YH
	push temp
	push temp2
	;**************FIRST ROW SCORE-BOARD *************
	ldi temp,'L'
	std Y+1, temp
	ldi temp,':'
	std Y+2, temp
	ldd temp, Y+LEVEL_VALUE
	std Y+3 , temp
	ldi temp,'C'
	std Y+5, temp
	ldi temp,':'
	std Y+6, temp
	ldd temp, Y+LIVES_VALUE
	std Y+LIVES, temp
	ldi temp,'|'
	std Y+8, temp
	ldi temp,'C'
	std Y+9, temp

	;**************SECOND ROW SCORE-BOARD *************
	ldi temp,'S'
	std Y+17, temp
	ldi temp,':'
	std Y+18, temp
	ldi temp,'|'
	std Y+24, temp



	;**************Positions of objects and data ***************
	ldi temp, 9
	std Y+CARPOS, temp 

	ldi temp, 'E' ;Assign E to each position to imply "empty"
	std Y+OBJ1POS, temp
	std Y+OBJ2POS, temp
	std Y+OBJ3POS, temp
	std Y+OBJ4POS, temp
	std Y+POW1POS, temp
	std Y+POW2POS, temp
	std Y+POW3POS, temp
	std Y+POW4POS, temp
	ldi temp, 'X' ;X implies the cap
	std Y+OBJCAP, temp

	ldi temp, 1
	std Y+GENOBJFLAG, temp

	;Reset the timers
	ldi temp, 0
	std Y+COUNTER4, temp ; clearing the counter values after counting 3597 interrupts which gives us one second
    std Y+COUNTER5, temp
    std Y+COUNTER6, temp
	std Y+COUNTER1, temp
    std Y+COUNTER2, temp
    std Y+COUNTER3, temp


	std Y+LEVELTIMER, temp

	std Y+PB1_COUNT, temp

	
	pop temp2
	pop temp
	pop YH
	pop YL
	pop ZH
	pop ZL

	ret


;********************************************************************************
;						LOGIC for motor
;*******************************************************************************
runMotor:
		; Bit 4 will function as OC2.
	ldi temp, 0xD8
	; the value controls the PWM duty cycle
	out OCR2, temp
	; Set the Timer2 to Phase Correct PWM mode.
	ldi temp, (1<< WGM20)|(1<<COM21)|(1<<CS20)
	out TCCR2, temp
	nop
	nop
ret

stopMotor:
		; Bit 4 will function as OC2.
	ldi temp, 0x00
	; the value controls the PWM duty cycle
	out OCR2, temp
	; Set the Timer2 to Phase Correct PWM mode.
	ldi temp, (1<< WGM20)|(1<<COM21)|(1<<CS20)
	out TCCR2, temp
	nop
	nop


	;Re initialising timer because motor and the internal timer both use tccr2
	ldi temp, 0b00000010 ;
	out TCCR2, temp


	;Init timer 0
	ldi temp, 0b00000010 ;
	out TCCR0, temp
	ldi temp, 1<<TOIE0 ; =278 microseconds
	ori temp, 1<<TOIE2

	out TIMSK, temp ; T/C0 interrupt enable
ret


;********************************************************************************
;				Logic for External interrupt 0 & 1 - Push button 0 & 1
;*******************************************************************************

	PB_0:
	rcall RESET
	reti

	PB_1:
	ldi temp, low(15000)
	ldi temp2, high(15000)
	delay ; delay for > 15ms
	delay
	sbis PIND, 1
	rcall PB_1_CONTENTS
	reti

	PB_1_CONTENTS:
	ldd temp, Y+PB1_COUNT
	inc temp
	std Y+PB1_COUNT, temp
	;rcall restartLevel
	ret


;********************************************************************************
;						Logic for Timer2 -  Level timer
;*******************************************************************************
;The following timer keeps track of each second. It is used for various things:
;Every 30 seconds, go to next level
;If push button 1 is pushed twice in a second, skip a level
;Run the motor for 2 seconds
Timer2:      
		            ; Prologue starts.
	push temp
	push r29                 ; Save all conflict registers in the prologue.
	push r28
	in temp, SREG
	push temp                 ; Prologue ends.
	/**** a counter for 3597 is needed to get one second-- Three counters are used in this example **************/                                          
	                         ; 3597  (1 interrupt 278microseconds therefore 3597 interrupts needed for 1 sec)
	ldd temp, Y+COUNTER4
	cpi temp, 97          ; counting for 97
	brne notsecond2

	ldd temp, Y+COUNTER5
	cpi temp, 35        ; counting for 35
	brne secondloop2          ; jumping into count 100 
	; DO STUFF AFTER EACH SECOND HERE:

	ldd temp, Y+MOTOR_FLAG ;If the player has crashed
	cpi temp, 1
	breq checkOffMotor 	;Go to check wether if the motor shoud be turned off



	ldd temp,Y+PB1_COUNT ; check the pressing of PB1
	cpi temp, 1 ;If its been pressed once in the second
	breq jumpToRestartLevel ;Restart the level
	cpi temp, 2 ;if twice or more
	brsh nextLevel ;Go to next level
	
	

	;If motor flag is off do this:		
	ldd temp, Y+LEVELTIMER
	cpi temp, 30
	breq nextLevel
	inc temp
	std Y+LEVELTIMER, temp
	rjmp outmot2


	;else if motor flag is on:
	checkOffMotor:
	ldd temp, Y+MOTOR_TIMER
	cpi temp, 3 ;This is 3 as we use the 1st second as a buffer for when the motor is triggered.
	breq motorTimeUp
	inc temp
	std Y+MOTOR_TIMER, temp
	

	rjmp outmot2 

	motorTimeUp: ;When the motor has run for 2 seconds
	clr temp
	std Y+MOTOR_TIMER, temp ;reset the counter for the motor to 0
	std Y+MOTOR_FLAG, temp ; reset motor flag

	rcall stopMotor
	ldd temp , Y+LIVES_VALUE
	dec temp
	cpi temp, '0' 
	breq toGameOver
	std Y+LIVES_VALUE, temp
	std Y+LIVES , temp

	jumpToRestartLevel:
	rcall restartLevel

	outmot2: ldi temp, 0
			std Y+COUNTER4, temp ; clearing the counter values after counting 3597 interrupts which gives us one second
	        std Y+COUNTER5, temp
	        std Y+COUNTER6, temp
	        rjmp exitTimer2        ; go to exit

	toGameOver:
	jmp gameOver

	notsecond2: inc temp   ; if it is not a second, increment the counter
			   std Y+COUNTER4, temp
	           rjmp exitTimer2

	secondloop2: ldd temp2, Y+COUNTER6
				inc temp2 ; counting 100 for every 35 times := 35*100 := 3500
				std Y+COUNTER6, temp2
	            cpi temp2,100 
	            brne exitTimer2
			    inc temp
				std Y+COUNTER5, temp
			    ldi temp2,0
				std Y+COUNTER6, temp2
	exitTimer2: 
	pop temp                  ; Epilogue starts;
	out SREG, temp           ; Restore all conflict registers from the stack.
	pop r28
	pop r29
	pop temp
	reti                     ; Return from the interrupt.

	;When player goes to next level this is triggered
	nextLevel:

	ldi temp,0
	std Y+PB1_COUNT, temp


	ldd temp, Y+ LEVEL_VALUE


	;Level 9 is cap
	cpi temp,'9'
	breq capLevel


	;If the level is lower than level 7. decrease the period speed by 0.1
	cpi temp, '7'
	brlo lowLevelPeriod

	cpi temp, '7'
	breq halfPeriod

	cpi temp, '8'
	breq level8Period

	;otherwise
	jmp halfPeriod ;half the level
	resumeNextLevel:
	 
	ldd temp, Y+LEVEL_VALUE
	inc temp
	std Y+LEVEL_VALUE, temp

	rcall restartLevel
		

	rjmp exitTimer2

	capLevel: ;Do nothing to the level.
	rjmp exitTimer2

	level8Period:
	ldi temp, 3
	std Y+PERIOD_HIGH, temp
	ldi temp, 60
	std Y+PERIOD_LOW ,temp

	jmp resumeNextLevel

	halfPeriod:
	ldd temp, Y+PERIOD_HIGH
	lsr temp
	std Y+PERIOD_HIGH, temp

	ldd temp, Y+PERIOD_LOW
	lsr temp
	std Y+PERIOD_LOW, temp

	jmp resumeNextLevel

	;Take away 597 because 0.1 of 3597
	lowLevelPeriod:

	ldd temp, Y+PERIOD_HIGH
	subi temp, 3
	std Y+PERIOD_HIGH, temp

	ldd temp, Y+PERIOD_LOW
	subi temp, 60
	std Y+PERIOD_LOW, temp

	jmp resumeNextLevel

	
		

;********************************************************************************
;						Logic for Timer0 - Period Timer
;*******************************************************************************


;The following timer keeps track of the period of the game. For example
;On level 1 the period will be 1 second
; Level 2 the period will be 0.9 seconds etc
	Timer0:                  ; Prologue starts.
	push temp
	push r29                 ; Save all conflict registers in the prologue.
	push r28
	in temp, SREG
	push temp                 ; Prologue ends.
	/**** a counter for 3597 is needed to get one second-- Three counters are used in this example **************/                                          
	                         ; 3597  (1 interrupt 278microseconds therefore 3597 interrupts needed for 1 sec)
	ldd temp, Y+COUNTER1
	ldd temp2, Y+PERIOD_LOW
	cp temp, temp2          ; counting for 97
	brne notsecond

	ldd temp, Y+COUNTER2
	ldd temp2, Y+PERIOD_HIGH
	cp temp, temp2         ; counting for 35
	brne secondloop          ; jumping into count 100 
	; DO STUFF AFTER EACH SECOND HERE:

	ldd temp, Y+CARPOS

	cpi temp, 'X'
	breq outmot

	rcall shiftObjects

	ldd temp, Y+GENOBJFLAG
	cpi temp, 1 ;Skip next line if temp is 0

	breq genObject

	ldi temp, 1
	std Y+GENOBJFLAG, temp

	resumeFromGenObject:

	rcall checkCollision

	rcall show_LCD


	rjmp outmot              ; jump to out put value


	outmot: ldi temp, 0
			std Y+COUNTER1, temp ; clearing the counter values after counting 3597 interrupts which gives us one second
	        std Y+COUNTER2, temp
	        std Y+COUNTER3, temp
	        rjmp exitTimer        ; go to exit



	notsecond: inc temp   ; if it is not a second, increment the counter
			   std Y+COUNTER1, temp
	           rjmp exitTimer

	secondloop: ldd temp2, Y+COUNTER3
				inc temp2 ; counting 100 for every 35 times := 35*100 := 3500
				std Y+COUNTER3, temp2
	            cpi temp2,100 
	            brne exitTimer
			    inc temp
				std Y+COUNTER2, temp
			    ldi temp2,0
				std Y+COUNTER3, temp2
	exitTimer: 
	pop temp                  ; Epilogue starts;
	out SREG, temp           ; Restore all conflict registers from the stack.
	pop r28
	pop r29
	pop temp
	reti                     ; Return from the interrupt.





	;*******************************************************************************************
	;									Obstacle logic
	;*********************************************************************************************

	;~~~~~~~~~~~~~~~~~~~~~~~~  OBSTACLE LOGIC~~~~~~~~~~~~~~~~~~~~~~~~~
	;Generate object if Y+GENOBJFLAG is on

	genObject:
	push YL
	push YH
	push ZL
	push ZH
	push temp

	rcall InitRandom
	rcall GetRandom ;Random number is now in register randomNum

	ldi temp, 0		;turn off generate random object flag i.e stop generating objstacles
	std Y+GENOBJFLAG, temp
	mov ZL, YL
	mov ZH, YH
	

	cpi randomNum, 26
	brlo genPow ;If number is less than 26 generate a power up
	cpi randomNum, 155
	brlo genObs ;if less than 155 and greater than 26, generate an obstacle
	;If number greater than 155 is generated, generate nothing and leave
	;the generate object flag as on.
	ldi temp, 1
	std Y+GENOBJFLAG, temp

	
	rjmp exitGenObj
	genObs:
	adiw Z, OBJ1POS	
	genObsLoop:
	ld temp, Z+
	cpi temp, 'E'
	brne genObsLoop  ;If branch here, means not reached an empty OBJPOS
	cpi randomNum, 91
	brlo ObsTop
	cpi randomNum, 155
	brlo ObsBottom



	genPow:
	adiw Z,POW1POS
	genPowLoop:
	ld temp, Z+
	cpi temp, 'E'
	brne genPowLoop  ;If branch here, means not reached an empty OBJPOS
	cpi randomNum, 13
	brlo PowerTop
	cpi randomNum, 26
	brlo PowerBottom

	exitGenObj:
	pop temp
	pop ZH
	pop ZL
	pop YH
	pop YL
	jmp resumeFromGenObject

	ObsTop: ;Generate obstacle in top row
	dec ZL
	ldi temp, 16
	std Z+0, temp
	ldi temp, 'O'
	std Y+16, temp

	jmp exitGenObj

	ObsBottom: ;Generate obstacle in bottom row
	dec ZL
	ldi temp, 32
	std Z+0, temp
	ldi temp, 'O'
	std Y+32, temp

	jmp exitGenObj

	PowerTop: ;Generate Power-up in top row
	dec ZL
	ldi temp, 16
	std Z+0, temp
	ldi temp, 'S'
	std Y+16, temp

 
	jmp exitGenObj

	PowerBottom: ;Generate obstacle in bottom row
	dec ZL
	ldi temp, 32
	std Z+0, temp
	ldi temp, 'S'
	std Y+32, temp
 
	jmp exitGenObj



	;~~~~~~~~~~~~~~~~~~~~~~~~SHIFTING OBSTACLE LOGIC~~~~~~~~~~~~~~~~~~~~~~~~~
	;Shifts each object in the stack (OBJ1POS to OBJ4POS)
	shiftObjects:
	push YL
	push YH
	push ZL
	push ZH
	push temp

	mov ZL, YL
	mov ZH, YH
	adiw Z, OBJ1POS


	shiftObjLoop:

	ld temp, Z+
	cpi temp, 'X'
	breq finishedShiftObj

	cpi temp, 'E'
	breq shiftObjLoop

	rcall shiftCurrentObject
	jmp shiftObjLoop

	finishedShiftObj:

	pop temp
	pop ZH
	pop ZL
	pop YH
	pop YL
	ret

	;Shift object which z is pointing to
	shiftCurrentObject:
	push r28
	push r29
	push ZL
	push ZH
	push temp
	push temp2
	push data

	subi ZL, 1 ;Move z back cuz its +1 from timer0

	push YH
	push YL

	add YL, temp
	ldd temp2, Y+0

	pop YL
	pop YH

	cpi temp2, 'S'
	breq checkPower	

	checkObstacle:
	cpi temp, 9 ;if obstacle position is 16 or 32, i.e at the edge of the board, do nothing
	breq destroyObstacle
	cpi temp, 25
	breq destroyObstacle
	jmp contShift

	destroyObstacle:
	push temp
	ldd temp, Y+LEVEL_VALUE
	subi temp, '0'
	rcall updateScore
	pop temp
	jmp destroyObj

	checkPower:
	cpi temp, 12
	breq destroyObj
	cpi temp, 28
	breq destroyObj

	contShift:
	dec temp
	st Z, temp

	mov ZL, YL
	mov ZH, YH
	add ZL, temp


	ldd temp, Z+1
	std Z+0, temp
	ldi temp, ' '
	std Z+1,temp




	resumeDestroy:
	pop data
	pop temp2
	pop temp
	pop ZH
	pop ZL
	pop r29
	pop r28

	ret

	destroyObj:
	push r28
	push r29
	push ZL
	push ZH

	add YL, temp

	ldi temp, ' '
	std Y+0, temp
	ldi temp, 'E'
	std Z+0, temp
	
	pop ZH
	pop ZL
	pop r29
	pop r28

	jmp resumeDestroy

	checkCollision:
	push count
	ldd temp, Y+CARPOS

	ldd temp2, Y+OBJ1POS
	cp temp,temp2
	breq toObsCollision

	ldd temp2, Y+OBJ2POS
	cp temp,temp2
	breq toObsCollision

	ldd temp2, Y+OBJ3POS
	cp temp,temp2
	breq toObsCollision

	ldd temp2, Y+OBJ4POS
	cp temp,temp2
	breq toObsCollision


	ldd temp2, Y+POW1POS
	ldi count, POW1POS
	cp temp,temp2
	breq handlePowCollision

	ldd temp2, Y+POW2POS
	ldi count, POW2POS
	cp temp,temp2
	breq handlePowCollision

	ldd temp2, Y+POW3POS
	ldi count, POW3POS
	cp temp,temp2
	breq handlePowCollision

	ldd temp2, Y+POW4POS
	ldi count, POW4POS
	cp temp,temp2
	breq handlePowCollision



 	resumeHandleCollision:
	pop count
	ret
	toObsCollision:
	rjmp handleObsCollision

	handlePowCollision:
	push ZL
	push ZH
	push temp
	push temp2
	mov ZL, YL
	mov ZH, YH


	rcall InitRandom
	rcall GetRandom ;Random number is stored in randomNum

	cpi randomNum, 128 ;Randomises what the powerups effect should be
	brlo livesPowerUp


;************ The Power up's effect increases the score by level*10********

	scorePowerUp:
	push temp2
	push temp
	ldd temp, Y+LEVEL_VALUE ;get the current level value
	subi temp, '0'
	mov temp3, temp
	mov temp2, temp

	;temp3 * 8
	lsl temp3
	lsl temp3
	lsl temp3

	;temp2 * 2
	lsl temp2

	;temp2 + temp3 = temp (LEVEL) * 10 
	add temp2,temp3

	mov temp,temp2

	rcall updateScore
	pop temp
	pop temp2
	rjmp powerUpEnd

;***********************************************************************

;************ The Power up's effect increases users lives by 1 with a cap at 9********

	livesPowerUp:
	push temp2
	push temp
	
	ldd temp, Y+LIVES_VALUE
	cpi temp, '9' ;Maximum lives is 9
	breq livesPowerUpEnd
	inc temp
	std Y+LIVES_VALUE,temp
	std Y+LIVES,temp

	livesPowerUpEnd:
	pop temp
	pop temp2

;***********************************************************************


	powerUpEnd:
	;temp is the car position
	;temp2 is powerup position
	;count is the position of the powerup stack
	add ZL, count
	ldi temp, 'E'

	std Z+0, temp


	mov ZL, YL
	mov ZH,YH

	add ZL, temp2
	ldi temp, 'C'
	std Z+0, temp

	pop temp2
	pop temp
	pop ZH
	pop ZL
	jmp resumeHandleCollision


	handleObsCollision:
	push ZL
	push ZH
	push temp
	push temp2
	mov ZL, YL
	mov ZH, YH
	ldd temp, Y+CARPOS
	add ZL, temp
	ldi temp, 'X'
	std Y+CARPOS, temp
	std Z+0, temp
	nop
	nop
	nop
	rcall runMotor
	
	ldi temp, 1
	std Y+MOTOR_FLAG, temp



	
	pop temp2
	pop temp
	pop ZH
	pop ZL
	jmp resumeHandleCollision


;Below is everything that happens when Lives reaches '0 i.e show game over screen and wait for reset.
	gameOver :
	rcall lcd_init
	rcall initBoard
	ldi temp, 'G'
	std Y+1 , temp
	ldi temp, 'A'
	std Y+2 , temp
	ldi temp, 'M'
	std Y+3 , temp
	ldi temp, 'E'
	std Y+4 , temp
	ldi temp, ' '
	std Y+5 , temp
	ldi temp, 'O'
	std Y+6 , temp
	ldi temp, 'V'
	std Y+7 , temp
	ldi temp, 'E'
	std Y+8 , temp
	ldi temp, 'R'
	std Y+9 , temp
	
	ldi temp, 'S'
	std Y+17 , temp
	ldi temp, ':'
	std Y+18 , temp
	
	rcall show_LCD
	;ret
	
	;Loop forever and turn off all interupts except for the reset interupt (PB0)
	;Then wait for reset
	GG:
	sei
	clr temp
	out TCCR0,temp
	out TCCR2, temp
	sts EICRA,temp
	out EIMSK,temp

	ldi temp, (1 << ISC01)
	sts EICRA, temp  
	in temp, EIMSK 
	ori temp, (1<<INT0)
	out EIMSK, temp

	rjmp GG



;********************************************************************************
;						Score logic
;*******************************************************************************

	updateScore:
		push temp ;temp holds our current value to be added
		push temp2  ;temp2 holds the value stored already
		push YL
		push YH
		push ZL
		push ZH

		ldi YL, low(RAMEND - STACKTOP)
		ldi YH, high(RAMEND - STACKTOP)
		mov ZL,YL
		mov ZH, YH
		clc
		;**********LED UPDATE LOGIC*********************
		add score, temp
		brcs incScore2
		
		resumeUpdateLED:

		;For PE2-PE7 first 6 bits of our score register
		push score
		lsl score
		lsl score
		andi score, (SCORE_MASK)

		out PORTE, score


		pop score
		
		;For PD4 and PD5 remaining 2 bits of our score register
		push score
		push score2
		andi score, SCORE_MASK2
		lsr score
		lsr score

		lsl score2
		lsl score2	
		lsl score2
		lsl score2
		lsl score2
		lsl score2

		or score,score2

		out PORTD, score
		pop score2
		pop score


		;for PE6 and PE7, the last two LED BARS (9 and 10)


		adiw Z, 23
		sbiw Z, 1

		ldd temp2, Z+1 ;Z is pointing to just below the UNITS
		subi temp2, '0'
		add temp2, temp

		updateLoop:
		cpi temp2, 10
		brsh handleDigit
		
		ldi temp, '0'
		add temp2,temp
		std Z+1, temp2
		clr temp2

		resumeUpdate:
		cpi temp2, 10
		brsh updateLoop

		
		pop ZH
		pop ZL
		pop YH
		pop YL
		pop temp2
		pop temp
	ret

	incScore2:
	inc score2
	rjmp resumeUpdateLED

	handleDigit:
	subi temp2, 10

	ldd temp, Z+0 ;load value from next (If units, then TENS)
	inc temp
	std Z+0, temp

	cpi temp2,10
	brsh handleDigit

	ldi temp, '0'
	add temp2,temp
	std Z+1, temp2
	sbiw Z,1
	ldd temp2, Z+1
	subi temp2, '0'
	jmp resumeUpdate

	;********************************************************************************
	;							Logic for random number
	;*******************************************************************************
	InitRandom:
	push randomNum ; save conflict register

	in randomNum, TCNT1L ; Create random seed from time of timer 1
	sts RAND,randomNum
	sts RAND+2,randomNum

	in randomNum,TCNT1H
	sts RAND+1, randomNum
	sts RAND+3, randomNum

	pop randomNum ; restore conflict register
	ret

	GetRandom:
	push r0 ; save conflict registers
	push r1
	push r17
	push r18
	push r19
	push r20
	push r21
	push r22

	clr r22 ; remains zero throughout

	ldi randomNum, low(RAND_C) ; set original value to be equal to C
	ldi r17, BYTE2(RAND_C)
	ldi r18, BYTE3(RAND_C)
	ldi r19, BYTE4(RAND_C)

	; calculate A*X + C where X is previous random number.  A is 3 bytes.
	lds r20, RAND
	ldi r21, low(RAND_A)
	mul r20, r21 ; low byte of X * low byte of A
	add randomNum, r0
	adc r17, r1
	adc r18, r22

	ldi r21, byte2(RAND_A)
	mul r20, r21  ; low byte of X * middle byte of A
	add r17, r0
	adc r18, r1
	adc r19, r22

	ldi r21, byte3(RAND_A)
	mul r20, r21  ; low byte of X * high byte of A
	add r18, r0
	adc r19, r1

	lds r20, RAND+1
	ldi r21, low(RAND_A)
	mul r20, r21  ; byte 2 of X * low byte of A
	add r17, r0
	adc r18, r1
	adc r19, r22

	ldi r21, byte2(RAND_A)
	mul r20, r21  ; byte 2 of X * middle byte of A
	add r18, r0
	adc r19, r1

	ldi r21, byte3(RAND_A)
	mul r20, r21  ; byte 2 of X * high byte of A
	add r19, r0

	lds r20, RAND+2
	ldi r21, low(RAND_A)
	mul r20, r21  ; byte 3 of X * low byte of A
	add r18, r0
	adc r19, r1

	ldi r21, byte2(RAND_A)
	mul r20, r21  ; byte 2 of X * middle byte of A
	add r19, r0

	lds r20, RAND+3
	ldi r21, low(RAND_A)	
	mul r20, r21  ; byte 3 of X * low byte of A
	add r19, r0

	sts RAND, randomNum ; store random number
	sts RAND+1, r17
	sts RAND+2, r18
	sts RAND+3, r19

	mov randomNum, r19  ; prepare result (bits 30-23 of random number X)
	lsl r18
	rol randomNum

	pop r22 ; restore conflict registers
	pop r21 
	pop r20
	pop r19
	pop r18
	pop r17
	pop r1
	pop r0
	ret

	;********************************************************************************
	;						Logic for LCD
	;*******************************************************************************


	lcd_write_com:
	push temp
	out PORTA, data ; set the data port's value up
	nop
	clr temp
	out PORTB, temp ; RS = 0, RW = 0 for a command write
	nop ; delay to meet timing (Set up time)
	sbi PORTB, LCD_E ; turn on the enable pin
	nop ; delay to meet timing (Enable pulse width)
	nop
	nop
	cbi PORTB, LCD_E ; turn off the enable pin
	nop ; delay to meet timing (Enable cycle time)
	nop
	nop
	pop temp
	ret


	;Function lcd_write_data: Write a character to the LCD. The data reg stores the value to be written.
	lcd_write_data:
	push temp
	out PORTA, data ; set the data port's value up
	ldi temp, 1 << LCD_RS
	out PORTB, temp ; RS = 1, RW = 0 for a data write
	nop ; delay to meet timing (Set up time)
	sbi PORTB, LCD_E ; turn on the enable pin
	nop ; delay to meet timing (Enable pulse width)
	nop
	nop
	cbi PORTB, LCD_E ; turn off the enable pin
	nop ; delay to meet timing (Enable cycle time)
	nop
	nop
	pop temp
	ret




	;Function lcd_wait_busy: Read the LCD busy flag until it reads as not busy.
	lcd_wait_busy:
	push temp
	clr temp
	out DDRA, temp ; Make PORTA be an input port for now
	out PORTA, temp
	ldi temp, 1 << LCD_RW
	out PORTB, temp ; RS = 0, RW = 1 for a command port read
	busy_loop:
	nop ; delay to meet timing (Set up time / Enable cycle time)
	sbi PORTB, LCD_E ; turn on the enable pin
	nop ; delay to meet timing (Data delay time)
	nop
	nop
	in temp, PINA ; read value from LCD
	cbi PORTB, LCD_E ; turn off the enable pin
	sbrc temp, LCD_BF ; if the busy flag is set
	rjmp busy_loop ; repeat command read
	clr temp ; else
	out PORTB, temp ; turn off read mode,
	ser temp
	out DDRA, temp ; make PORTA an output port again
	pop temp
	ret ; and return
	; Function delay: Pass a number in registers r18:r19 to indicate how many microseconds
	; must be delayed. Actual delay will be slightly greater (~1.08us*r18:r19).
	; r18:r19 are altered in this function.
	; Code is omitted
	;Function lcd_init Initialisation function for LCD.



	lcd_init:
	push temp
	push temp2
	ser temp
	out DDRA, temp ; PORTAh, the data port is usually all otuputs
	out DDRB, temp ; PORTB, the control port is always all outputs
	ldi temp, low(15000)
	ldi temp2, high(15000)
	delay ; delay for > 15ms
	; Function set command with N = 1 and F = 0
	ldi data, LCD_FUNC_SET | (1 << LCD_N)
	rcall lcd_write_com ; 1st Function set command with 2 lines and 5*7 font
	rcall lcd_wait_busy ; Wait until the LCD is ready
	ldi temp, low(4100)
	ldi temp2, high(4100)
	delay ; delay for > 4.1ms
	ldi data, LCD_FUNC_SET | (1 << LCD_N)
	rcall lcd_write_com ; 2nd Function set command with 2 lines and 5*7 font
	rcall lcd_wait_busy ; Wait until the LCD is ready
	ldi temp, low(100)
	ldi temp2, high(100)
	delay ; delay for > 100us
	ldi data, LCD_FUNC_SET | (1 << LCD_N)
	rcall lcd_write_com ; 3rd Function set command with 2 lines and 5*7 font
	rcall lcd_wait_busy ; Wait until the LCD is ready
	ldi data, LCD_FUNC_SET | (1 << LCD_N)
	rcall lcd_write_com ; Final Function set command with 2 lines and 5*7 font
	rcall lcd_wait_busy ; Wait until the LCD is ready
	ldi data, LCD_DISP_OFF
	rcall lcd_write_com ; Turn Display off
	rcall lcd_wait_busy ; Wait until the LCD is ready
	ldi data, LCD_DISP_CLR
	rcall lcd_write_com ; Clear Display
	rcall lcd_wait_busy ; Wait until the LCD is ready
	; Entry set command with I/D = 1 and S = 0
	ldi data, LCD_ENTRY_SET | (1 << LCD_ID)
	rcall lcd_write_com ; Set Entry mode: Increment = yes and Shift = no
	rcall lcd_wait_busy ; Wait until the LCD is ready
	; Display on command with C = 0 and B = 1
	ldi data, LCD_DISP_ON | (1 << LCD_C)
	rcall lcd_write_com ; Trun Display on with a cursor that doesn't blink
	pop temp2
	pop temp
	ret

	;*****************************************************************************************
	; 							Logic for main and Keyboard
	;*****************************************************************************************

	 .equ LENGTH = 16

	main:
	clr data
	clr temp

	ldi mask, INITCOLMASK ; initial column mask
	clr col ; initial column
	colloop:
	out PORTC, mask ; set column to mask value
	; (sets column 0 off)
	ldi temp, 0xFF ; implement a delay so the
	; hardware can stabilize

	delayKeypad:
	dec temp
	brne delayKeypad
	ldi temp, low(300)
	ldi temp2, high(300)
	delay ; delay for > 4.1ms
	;rcall buttonDelay
	;rcall buttonDelay
	in temp, PINC ; read PORTC
	andi temp, ROWMASK ; read only the row bits
	cpi temp, 0xF ; check if any rows are grounded
	breq nextcol ; if not go to the next column
	ldi mask, INITROWMASK ; initialise row check
	clr row ; initial row


	rowloop:
	mov temp2, temp
	and temp2, mask ; check masked bit
	brne skipconv ; if the result is non-zero,
	; we need to look again

	rcall convert ; if bit is clear, convert the bitcode
	jmp main ; and start again



	skipconv:
	inc row ; else move to the next row
	lsl mask ; shift the mask to the next bit
	jmp rowloop
	nextcol:
	cpi col, 3 ; check if we�re on the last column
	breq main ; if so, no buttons were pushed,
	; so start again.

	sec ; else shift the column mask:
	; We must set the carry bit
	rol mask ; and then rotate left by a bit,
	; shifting the carry into
	; bit zero. We need this to make
	; sure all the rows have
	; pull-up resistors
	inc col ; increment column value
	jmp colloop ; and check the next column
	; convert function converts the row and column given to a
	; binary number and also outputs the value to PORTC.
	; Inputs come from registers row and col and output is in
	; temp.

	convert:
	;numbers:
	mov temp, row ; otherwise we have a number (1-9)
	lsl temp ; temp = row * 2
	add temp, row ; temp = row * 3
	add temp, col ; add the column address
	; to get the offset from 1
	inc temp ; add 1. Value of switch is


	convert_end:
	;	ldi temp2, 48
	;	add temp,temp2

	;	rcall lcd_init
		
		;mov data,temp
	;	rcall lcd_wait_busy
	   ; rcall lcd_write_data            ; write the character to the screen 
		push YL
		push YH
		push ZL
		push ZH
		push temp

		;Check if car position is X

		ldd temp2, Y+CARPOS

		cpi temp2, 'X'
		breq resumePos

		cpi temp, 6
		breq goRight

		cpi temp, 4
		breq goLeft

		cpi temp, 2
		breq goUp

		cpi temp, 8
		breq goDown



		resumePos:
		pop temp
		pop ZH
		pop ZL
		pop YH
		pop YL


		rcall show_LCD
		;Then long delay so keypad doesnt get triggered when held down
		ret

	;*******************************************************************************************
	;									CONTROLS
	;*********************************************************************************************

	goRight:


		ldd temp, Y+CARPOS

		cpi temp, 16 ;if car position is 16 or 32, i.e at the edge of the board, do nothing
		breq resumePos
		cpi temp, 32
		breq resumePos

		mov ZL,YL
		mov ZH, YH

		add ZL, temp
	

		ldi temp, ' '
		std Z+0, temp
		ldi temp, 'C'
		std Z+1,temp

		ldd temp, Y+CARPOS	
		inc temp
		std Y+CARPOS, temp


		rcall checkCollision
		jmp resumePos


	goLeft:




		ldd temp, Y+CARPOS

		cpi temp, 9 ;if car position is 16 or 32, i.e at the edge of the board, do nothing
		breq resumePos
		cpi temp, 25
		breq resumePos

		mov ZL,YL
		mov ZH, YH
		dec temp
		add ZL, temp
		ldi temp, 'C'
		std Z+0, temp
		ldi temp, ' '
		std Z+1,temp

		ldd temp, Y+CARPOS	
		dec temp
		std Y+CARPOS, temp

		rcall checkCollision
	jmp resumePos


	goUp:


		ldd temp, Y+CARPOS

		cpi temp, 17
		brlo resumePos

		mov ZL,YL
		mov ZH, YH
		subi temp, 16 ;Go up a row

		add ZL, temp
		ldi temp, 'C'
		std Z+0, temp
		ldi temp, ' '
		std Z+16,temp

		ldd temp, Y+CARPOS	
		subi temp, 16
		std Y+CARPOS, temp
		rcall checkCollision
		jmp resumePos
	
	goDown:


		ldd temp, Y+CARPOS
		cpi temp, 24
		brsh resumePos
		mov ZL,YL
		mov ZH, YH

		add ZL, temp
		ldi temp, ' '
		std Z+0, temp
		ldi temp, 'C'
		std Z+16,temp

		ldd temp, Y+CARPOS	
		ldi temp2, 16
		add temp,temp2
		std Y+CARPOS, temp
		rcall checkCollision

		jmp resumePos



	;*******************************************************************************************
	;									SHOW LCD
	;*********************************************************************************************
	show_LCD:
		push r28
		push r29
		push ZL
		push ZH
		mov ZL, YL
		mov ZH, YH
		rcall lcd_init ;Clean board
		mov data,temp

		adiw ZL, 1

		ldi count, LENGTH               ; initialise counter 
	show_LCD_loop: 
	
	        ld data, Z+                 ; read a character from the string 

	        rcall lcd_wait_busy
	        rcall lcd_write_data            ; write the character to the screen
	        dec count                       ; decrement character counter

	        brne show_LCD_loop                  ; loop again if there are more characters
		
			rcall lcd_wait_busy
	        ldi data, LCD_ADDR_SET | LCD_LINE2
	        rcall lcd_write_com                     ; move the insertion point to start of line 2

	        ldi count, LENGTH                       ; initialise counter 
	show_LCD_loop2: 


	        ld data, Z+                 	; read a character from the string 		
	       	rcall lcd_wait_busy
	        rcall lcd_write_data            ; write the character to the screen
		    dec count                       ; decrement character counter                               ; decrement character counter
	        brne show_LCD_loop2                         ; loop again if there are more characters
	exit:
		pop ZH
		pop ZL
		pop r29
		pop r28
		ret
